# Use an OpenJDK runtime as a parent image
FROM openjdk:17-slim

# Create a user and group used to run the application
RUN addgroup --system appgroup && adduser --system --group appuser --no-create-home

# Set the working directory to /app
WORKDIR /app

# Ensure the application runs with user permissions
RUN chown appuser:appgroup /app
USER appuser

# Copy the jar into the container at /app
ARG JAR_FILE
COPY --chown=appuser:appgroup ${JAR_FILE} app.jar

# Make port 80 available to the world outside this container
EXPOSE 80

# Run the jar file 
ENTRYPOINT ["java", "-jar", "app.jar"]
