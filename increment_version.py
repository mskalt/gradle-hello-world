import os
import subprocess

def run_command(command):
    """Runs a given command through subprocess and captures the output."""
    try:
        result = subprocess.run(command, check=True, text=True, capture_output=True)
        print(result.stdout)
        return result.stdout
    except subprocess.CalledProcessError as e:
        print(f"Command '{' '.join(command)}' failed with return code {e.returncode}")
        print(e.stderr)
        raise

def commit_changes(file_path, new_version):
    """Commits the changes using Git and handles authentication with a token."""
    token = os.getenv('GITLAB_PERSONAL_TOKEN')
    print("DEBUG: GITLAB_PERSONAL_TOKEN:", token)  # Debugging line to check token visibility
    if not token:
        raise Exception("No GITLAB_PERSONAL_TOKEN found. Please set this variable in your CI/CD settings.")
    
    # Set the header for authorization and update the origin URL to include the token
    run_command(['git', 'config', '--global', 'http.extraHeader', f"Authorization: Bearer {token}"])
    origin_url = f"https://oauth2:{token}@gitlab.com/your-repository-path.git"
    run_command(['git', 'remote', 'set-url', 'origin', origin_url])

    # Git operations
    run_command(['git', 'add', file_path])
    commit_output = run_command(['git', 'commit', '-m', f'Increment version to {new_version}'])
    print("Commit output:", commit_output)
    push_output = run_command(['git', 'push', 'origin', 'HEAD:refs/heads/main'])
    print("Push output:", push_output)

if __name__ == "__main__":
    file_path = 'build.gradle.kts'  # Ensure this path is correct
    current_version = '1.0.0'
    new_version = '1.0.1'
    commit_changes(file_path, new_version)
